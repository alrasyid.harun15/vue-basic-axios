<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([  'namespace'=>'hris',
                'prefix'=>'hris'], function(){
    Route::get('','BiodataController@index');
    Route::post('store','BiodataController@store');
    Route::get('{id}/show','BiodataController@show');
    Route::post('patch','BiodataController@patch');
    Route::delete('{id}/delete','BiodataController@destroy');
    });
