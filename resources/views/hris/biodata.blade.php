<html>

	<head>
		<title>Laravel Vue.js</title>
		<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">		
	</head>
	
	<body>
		
		<div id="hrisApp" class="flex flex-col mt-5 ml-5 mr-5 mb-5">
			<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div class="shadow overflow-hidden border-b border-gray-300 sm:rounded-lg">
						<h3 class="mt-3 ml-3 text-lg font-medium leading-6 text-gray-900">@{{ message }}</h3>
  			
              			<div class="mt-6 ml-6 text-sm leading-5 text-gray-600">	  					
              				<div v-if="addOrEdit" >
              					<p><strong>Tambah biodata</strong></p>	
                  				<input type="text" v-model="nama" class="shadow appearance-none border rounded py-2 px-3 text-gray leading-tight focus:outline-none focus:shadow-outline">
                  				<button v-on:click="store()" class="bg-blue-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Simpan</button>
              				</div>
              				<div v-if="!addOrEdit">
              					<p><strong>Ubah biodata</strong></p>
              					<input type="text" v-model="nama" class="shadow appearance-none border rounded py-2 px-3 text-gray leading-tight focus:outline-none focus:shadow-outline">
                  				<button v-on:click="patch(bio)" class="bg-blue-500 hover:bg-orange-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Ubah</button>
              				</div>
              			</div>
              			
              			<table class="m-5 align-middle  divide-y divide-gray-200 border">
              				<thead>
                  				<tr>
                  					<th class="px-6 py-3 bg-green-150 text-left text-xs leading-4 font-medium text-dark-500 uppercase ">No</th>
                  					<th class="px-6 py-3 bg-green-150 text-left text-xs leading-4 font-medium text-dark-500 uppercase ">Nama</th>
                  					<th class="px-6 py-3 bg-green-150 text-left text-xs leading-4 font-medium text-dark-500 uppercase ">Action</th>
                  				<tr>	
              				</thead>
              				<tbody class="divide-y divide-gray-200">
              				<tr v-for="(biodata,index) in biodatas">
              					<td class="px-3 py-2 ">@{{index+1}}</td>
              					<td class="px-3 py-2 ">@{{biodata.nama}}</td>
              					<td class="px-3 py-2 ">
              						<button v-on:click="show(biodata,index)" class="bg-blue-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Ubah</button>
              						<button v-on:click="destroy(biodata, index)" class="bg-blue-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Hapus</button>
              					</td>
              				</tr>
              				</tbody>
              			</table>
						
					</div>
				</div>
			</div>
		</div>
		
	</body>
	
	<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script src="{{URL('js/biodata.js')}}">
	</script>	

</html>