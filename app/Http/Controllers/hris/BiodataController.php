<?php

namespace App\Http\Controllers\hris;

use App\Http\Requests\hris\BiodataRequest;
use App\Http\Resources\hris\BiodataResource;
use App\hris\Biodata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biodatas = Biodata::all();
        
        return BiodataResource::collection($biodatas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BiodataRequest $request)
    {
        $bio = Biodata::create([
            'nama'=>$request['nama']
        ]);
        
        return new BiodataResource($bio);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bio = Biodata::findOrFail($id);
        
        if(!is_null($bio)){
            
            return new BiodataResource($bio);
            
        }
        
        return response()->json([
            'response_code'=>'01',
            'response_message'=>'data tidak ditemukan!'
        ],200);
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function patch(Request $request)
    {   
        
        $id = $request['id'];
        
        $bio = Biodata::find($id);
        
        if( !is_null($id) && !is_null($bio)){
            
            Biodata::query()
            ->where("id","=",$request['id'])
            ->update([
                'nama'=>$request['nama']
            ]);
            
            $bio = Biodata::find($id);
            
            return new BiodataResource($bio);
            
        }else{
            
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'data tidak ditemukan!'
            ],200);
            
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Biodata::destroy($id);
        
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'data berhasil dihapus!'
        ],200);
    }
    
}
