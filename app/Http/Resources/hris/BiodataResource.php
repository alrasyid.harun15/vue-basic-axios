<?php

namespace App\Http\Resources\hris;

use Illuminate\Http\Resources\Json\JsonResource;

class BiodataResource extends JsonResource
{
       /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        return [
            'id'=>$this->id,
            'nama'=>$this->nama
        ];
        
    }

    public function with($request)
    {
        return [
            'response_code'=>'00',
            'response_message'=>'Proses Berhasil',
        ];
    }
}
