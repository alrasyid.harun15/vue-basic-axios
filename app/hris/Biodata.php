<?php

namespace App\hris;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $guarded = [];
    protected $table = 'biodatas';
}
