<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\hris\Biodata;
use Faker\Generator as Faker;

$factory->define(Biodata::class, function (Faker $faker) {
    return [
        'nama'=>$faker->name('female')
    ];
});
