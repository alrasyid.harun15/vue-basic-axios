<?php

use Illuminate\Database\Seeder;
use App\hris\Biodata;

class BiodataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Factory(Biodata::class,5)->create();
    }
}
