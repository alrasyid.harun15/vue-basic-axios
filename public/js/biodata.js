		
var app = new Vue({
	el: '#hrisApp',
	data: {
		message: 'Aplikasi Biodata Sederhana!',
		biodatas:[],
		biodata:'',
		nama:'',
		addOrEdit:true,
	},
	methods:{
		store:function(){
			console.log(`To be saved name = ${this.nama}` )

			let newBio = Biodata(this.nama);

			axios.post('./api/hris/store',newBio).then(
			response => {
					console.log(response.data)

					let code = response.data.response_code
					let persistedBio = response.data.data 
					
					if(code==="00"){
						this.biodatas.push(persistedBio)
						this.nama=''
						alert(`Biodata ${this.nama} berhasil ditambah!` )
						this.addOrEdit=true
					}else{
						alert(`Biodata ${this.nama} tidak berhasil ditambah!` )
					}
				}
			)


			
		},
		show:function(bio, id){
			console.log(`To be shown index = ${id} with id : ${bio.id}` )

			axios.get(`./api/hris/${bio.id}/show`).then(
			response => {
					console.log(response.data)

					let code = response.data.response_code
					let bioFromServer = response.data.data

					if(code==="00"){
						this.addOrEdit=false
						if(this.biodatas[id].nama!==bioFromServer.nama){
							alert('Data Tidak Sinkron antara web dan DB, mohon refresh halaman ini!' )
						}else{
							this.nama = bioFromServer.nama
							this.biodatas[id]=bioFromServer
							this.bio = this.biodatas[id] 
						}
						
					}else{
						alert(`Data Biodata tidak tersedia!` )
					}
				}
			)						
		},
		patch:function(bio){
			console.log(`To be patch bio = ${this.nama}` )

			this.bio.nama = this.nama

			axios.post('./api/hris/patch',bio).then(
			response => {
					console.log(response.data)

					let code = response.data.response_code

					if(code==="00"){						
						this.addOrEdit=true
						alert(`Biodata ${this.nama} berhasil diubah!` )
						this.nama=''
						this.bio=''
					}else{
						alert(`Biodata ${this.nama} tidak berhasil diubah!` )
					}
				}
			)
					
		},
		destroy:function(bio, id){
			console.log(`To be deleted bio = ${bio.nama}` )
			confirmed = confirm(`Yakin akan hapus Biodata ${bio.nama} ?` )
			if(confirmed)

				axios.delete(`./api/hris/${bio.id}/delete`).then(
				response => {
						console.log(response.data)

						let code = response.data.response_code

						if(code==="00"){
							this.biodatas.splice(id,1)
							alert(`Biodata ${bio.nama} berhasil dihapus!` )
							this.nama=''
							this.bio=''
							this.addOrEdit=true
						}else{
							alert(`Biodata ${bio.nama} tidak berhasil dihapus!` )
						}
					}
				)
			else
				alert(`Biodata ${bio.nama} tidak dihapus!` )	
		}			
	},	
	mounted:function(){
		console.log("page mounted!" )

		axios.get('./api/hris').then(
			response => {
					console.log(response.data)
					this.biodatas = response.data.data
				}
			)

	}
})


function Biodata(nama){
	return {
		nama:nama
	}
}
